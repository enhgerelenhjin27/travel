import {LOGIN, LOGOUT, USER_DATA} from '@actions/auth';

const initialAuth = {
  isAuth: false,
  userData: null,
  loggedUser: null,
};

const reducerAuth = (state, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        isAuth: true,
        loggedUser: action.list,
      };
    case LOGOUT:
      return {
        ...state,
        isAuth: false,
      };
    case USER_DATA:
      return {
        ...state,
        userData: action.list,
      };
    default:
      return state;
  }
};

export default [reducerAuth, initialAuth];
