import combineReducers from 'react-combine-reducers';

import user from './user';
import testList from './testList';

const [reducer, initialState] = combineReducers({
  user,
  testList,
});

export {reducer, initialState};
