import {
  SET_TEST_LIST,
  SET_TEST_COUNT_NUM,
  SET_DASHBOARD_DATA,
} from '../actions/testList';

const initialAuth = {
  testList: [],
  myOwnedCardList: [],
  answeredTestCount: 0,
  dashboardData: [],
};

const reducerAuth = (state, action) => {
  switch (action.type) {
    case SET_TEST_LIST:
      return {
        ...state,
        testList: action.list,
      };
    case SET_TEST_COUNT_NUM:
      return {
        ...state,
        answeredTestCount: action.list,
      };
    case SET_DASHBOARD_DATA:
      return {
        ...state,
        dashboardData: action.list,
      };
    default:
      return state;
  }
};

export default [reducerAuth, initialAuth];
