import React ,{Component} from 'react';
import {Router ,Stack, Scene} from 'react-native-router-flux';
import Login from '../components/Login/index';
import SignUP from '../components/SignUp/SignUp';
import AppNavigation from '../navigations/AppNavigation';
 
const  Router1=()=>{

    return(
        <Router>
            <Stack key='root' hideNavBar={true}>
                <Stack key ='login' component={Login} title="Login"/>
                <Stack key ='signUP' component={SignUP} title="SignUP"/>
                <Stack key ='home' component={AppNavigation} title="home"/>
            </Stack>
        </Router>
    )

} 

export default Router1;