export function home(color="white"){
    return  `<svg height="512" viewBox="0 0 128 128" width="512" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><linearGradient id="a" gradientUnits="userSpaceOnUse" x1="100.356" x2="27.644" y1="116.106" y2="43.395"><stop offset="0" stop-color="#8a21ed"/><stop offset="1" stop-color="#fc68c0"/></linearGradient>
	<path fill=${color}  d="m17.25 82.767v-.048a1.75 1.75 0 0 1 3.5 0 1.771 1.771 0 0 1 -1.75 1.774 1.73 1.73 0 0 1 -1.75-1.726zm88.65-33.74-35.824-29.592a9.555 9.555 0 0 0 -12.152 0l-35.824 29.592a13.306 13.306 0 0 0 -4.85 10.291v13.013a1.75 1.75 0 0 0 3.5 0v-13.013a9.815 9.815 0 0 1 3.576-7.591l35.826-29.593a6.047 6.047 0 0 1 7.7 0l35.826 29.593a9.815 9.815 0 0 1 3.576 7.591v42.841a5.1 5.1 0 0 1 -5.091 5.091h-22.371v-25.6a10.261 10.261 0 0 0 -10.249-10.25h-11.086a10.261 10.261 0 0 0 -10.249 10.247v25.6h-22.367a5.1 5.1 0 0 1 -5.091-5.091v-8.23a1.75 1.75 0 0 0 -3.5 0v8.233a8.6 8.6 0 0 0 8.591 8.591h24.117a1.75 1.75 0 0 0 1.75-1.75v-27.353a6.757 6.757 0 0 1 6.749-6.747h11.086a6.757 6.757 0 0 1 6.749 6.749v27.351a1.75 1.75 0 0 0 1.75 1.75h24.117a8.6 8.6 0 0 0 8.591-8.591v-42.841a13.306 13.306 0 0 0 -4.85-10.291z" fill="url(#a)"/>
	</svg>`
}
export function heart(color){
    return  `<svg id="color" enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512" xmlns="http://www.w3.org/2000/svg">
	<path fill=${color} d="m11.466 22.776c.141.144.333.224.534.224s.393-.08.534-.224l9.594-9.721c4.001-4.053 1.158-11.055-4.532-11.055-3.417 0-4.985 2.511-5.596 2.98-.614-.471-2.172-2.98-5.596-2.98-5.672 0-8.55 6.984-4.531 11.055z" fill="#f44336"/></svg>`
}
export function user(color="white"){
    return  `<?xml version="1.0" encoding="iso-8859-1"?>
	<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
	<g>
		<g>
			<path fill=${color} d="M256,288.389c-153.837,0-238.56,72.776-238.56,204.925c0,10.321,8.365,18.686,18.686,18.686h439.747
				c10.321,0,18.686-8.365,18.686-18.686C494.56,361.172,409.837,288.389,256,288.389z M55.492,474.628
				c7.35-98.806,74.713-148.866,200.508-148.866s193.159,50.06,200.515,148.866H55.492z"/>
		</g>
	</g>
	<g>
		<g>
			<path fill=${color} d="M256,0c-70.665,0-123.951,54.358-123.951,126.437c0,74.19,55.604,134.54,123.951,134.54s123.951-60.35,123.951-134.534
				C379.951,54.358,326.665,0,256,0z M256,223.611c-47.743,0-86.579-43.589-86.579-97.168c0-51.611,36.413-89.071,86.579-89.071
				c49.363,0,86.579,38.288,86.579,89.071C342.579,180.022,303.743,223.611,256,223.611z"/>
		</g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	</svg>
	`
}
export function log_out(color="gray"){
    return  `<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="84.001px" height="68px" viewBox="0 0 84.001 68" enable-background="new 0 0 84.001 68" xml:space="preserve">
	<g>
		<path fill=${color} d="M53,47.891c-2.762,0-5,2.238-5,5c0,0.035,0.01,0.068,0.012,0.103L48,53c0,2.757-2.242,5-5,5H15
			c-2.758,0-5-2.243-5-5V15c0-2.757,2.242-5,5-5h28c2.758,0,5,2.243,5,5l0.02,0.021c0.098,2.674,2.281,4.818,4.98,4.818
			c2.701,0,4.887-2.146,4.982-4.822L58,15c0-0.036-0.004-0.07-0.006-0.107C57.996,14.875,58,14.857,58,14.84
			c0-0.056-0.014-0.107-0.016-0.162C57.811,6.544,51.176,0,43,0H15C6.715,0,0,6.716,0,15v38c0,8.284,6.715,15,15,15h28
			c8.285,0,15-6.716,15-15l-0.01-0.01c0-0.033,0.01-0.065,0.01-0.1C58,50.129,55.762,47.891,53,47.891z"/>
		<path fill=${color} d="M83.904,33.02c-0.027-0.139-0.08-0.267-0.119-0.4c-0.053-0.18-0.094-0.359-0.166-0.533
			c-0.064-0.156-0.154-0.297-0.234-0.445c-0.074-0.139-0.137-0.283-0.225-0.416c-0.164-0.246-0.355-0.472-0.559-0.684
			c-0.023-0.024-0.039-0.053-0.063-0.076L71.932,19.857c-1.953-1.953-5.119-1.952-7.072,0c-1.951,1.953-1.953,5.119,0,7.072
			L66.932,29h-30.93c-2.762,0-5,2.238-5,5s2.238,5,5,5h30.93l-2.072,2.07c-1.953,1.953-1.951,5.119,0,7.072
			c1.953,1.952,5.119,1.953,7.072,0l10.607-10.607c0.023-0.023,0.039-0.052,0.063-0.076c0.203-0.212,0.395-0.438,0.559-0.684
			c0.088-0.133,0.15-0.277,0.225-0.416c0.08-0.148,0.17-0.289,0.234-0.445c0.072-0.174,0.113-0.354,0.166-0.533
			c0.039-0.134,0.092-0.262,0.119-0.4C84.033,34.334,84.033,33.666,83.904,33.02z"/>
	</g>
	</svg>`
}
export function userInfo(color="white"){
    return  `<?xml version="1.0" encoding="iso-8859-1"?>
	<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
	<g>
		<g>
			<path fill=${color} d="M256,288.389c-153.837,0-238.56,72.776-238.56,204.925c0,10.321,8.365,18.686,18.686,18.686h439.747
				c10.321,0,18.686-8.365,18.686-18.686C494.56,361.172,409.837,288.389,256,288.389z M55.492,474.628
				c7.35-98.806,74.713-148.866,200.508-148.866s193.159,50.06,200.515,148.866H55.492z"/>
		</g>
	</g>
	<g>
		<g>
			<path  fill=${color}d="M256,0c-70.665,0-123.951,54.358-123.951,126.437c0,74.19,55.604,134.54,123.951,134.54s123.951-60.35,123.951-134.534
				C379.951,54.358,326.665,0,256,0z M256,223.611c-47.743,0-86.579-43.589-86.579-97.168c0-51.611,36.413-89.071,86.579-89.071
				c49.363,0,86.579,38.288,86.579,89.071C342.579,180.022,303.743,223.611,256,223.611z"/>
		</g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	<g>
	</g>
	</svg>
	`
}
export function search(color = '#4F5864') {
	return`<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="28.996px" height="28.997px" viewBox="0 0 28.996 28.997" enable-background="new 0 0 28.996 28.997" xml:space="preserve">
	<path fill=${color} d="M28.269,24.734l-5.344-5.344C24.233,17.415,25,15.049,25,12.5C25,5.598,19.403,0,12.5,0S0,5.598,0,12.5
		C0,19.404,5.597,25,12.5,25c2.547,0,4.914-0.766,6.89-2.074l5.344,5.344c0.972,0.973,2.552,0.97,3.528-0.007
		S29.24,25.707,28.269,24.734z M5,12.5C5,8.365,8.364,5,12.5,5S20,8.365,20,12.5c0,4.137-3.364,7.5-7.5,7.5S5,16.637,5,12.5z"/>
	</svg>
	`;
}
export function login_user(color) {
	return `<?xml version="1.0" encoding="utf-8"?>
	  <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	  <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	   width="42.995px" height="42.938px" viewBox="0 0 42.995 42.938" enable-background="new 0 0 42.995 42.938" xml:space="preserve">
	  <g>
	  <g>
	  <path fill=${color} d="M17,22h9c3.866,0,7-3.14,7-7.014V7.013C33,3.14,29.866,0,26,0h-9c-3.866,0-7,3.14-7,7.013v7.973
	  C10,18.86,13.134,22,17,22z M15,7.013C15,5.903,15.897,5,17,5h9c1.103,0,2,0.903,2,2.013v7.973C28,16.097,27.103,17,26,17h-9
	  c-1.103,0-2-0.903-2-2.014V7.013z"/>
	  <path fill=${color} d="M28.995,25H14C6.295,25,0.048,31.225,0.004,38.918C0.004,38.926,0,38.932,0,38.939V39v2.937
	  c0,0.553,0.447,1.001,0.998,1.001h12.005c0.551,0,0.998-0.448,0.998-1.001v-2.998c0-0.553-0.447-1.001-0.998-1.001H5.068
	  C5.597,33.475,9.397,30,14,30h14.995c4.603,0,8.403,3.475,8.932,7.938H22.995c-0.552,0-1,0.531-1,1.187v2.627
	  c0,0.655,0.448,1.187,1,1.187h19c0.552,0,1-0.531,1-1.187v-2.627V39C42.995,31.268,36.727,25,28.995,25z"/>
	  </g>
	  </g>
	  </svg>
	  `;
  }
  export function login_password(color) {
	return `<?xml version="1.0" encoding="utf-8"?>
	  <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	  <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  \t width="46.997px" height="23px" viewBox="0 0 46.997 23" enable-background="new 0 0 46.997 23" xml:space="preserve">
	  <path fill=${color} d="M39.997,0h-9c-3.866,0-7,3.134-7,7v2H1.003c-0.552,0-1,0.531-1,1.187v2.627c0,0.655,0.448,1.187,1,1.187h5
	  \tv3.813c0,0.655,0.448,1.187,1,1.187h5c0.552,0,1-0.531,1-1.187V14h10.994v2c0,3.866,3.134,7,7,7h9c3.866,0,7-3.134,7-7V7
	  \tC46.997,3.134,43.863,0,39.997,0z M41.997,16c0,1.103-0.897,2-2,2h-9c-1.103,0-2-0.897-2-2V7c0-1.103,0.897-2,2-2h9
	  \tc1.103,0,2,0.897,2,2V16z"/>
	  </svg>`;
  }
  export function email(color) {
	return `<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="57.996px" height="41.995px" viewBox="0 0 57.996 41.995" enable-background="new 0 0 57.996 41.995" xml:space="preserve">
	<g>
		<path fill=${color} d="M57.993,12.769L28.998,28.406L0.003,12.769v1.417L0,14.182v22.813c0,2.762,2.239,5,5,5h0.003h47.989h0.003
			c2.761,0,5-2.238,5-5V14.182l-0.003,0.004V12.769z"/>
		<path  fill=${color} d="M29,22.022L57.995,6.384L57.996,5c0-2.761-2.238-5-5-5H5.003c-2.761,0-5,2.239-5,5l0.002,1.384L29,22.022z"
			/>
	</g>
	</svg>`;
  }
  export function pass(color) {
	return`<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="59.833px" height="47.994px" viewBox="0 0 59.833 47.994" enable-background="new 0 0 59.833 47.994" xml:space="preserve">
	<g>
		<path fill="#ED1C24" d="M54.833,15l0.042,5.875L29.917,33.188L4.958,20.875L5,15l-5,3.125v24.869c0,2.761,2.239,5,5,5h24.917
			h24.917c2.761,0,5-2.239,5-5V18.125L54.833,15z"/>
		<path fill="#ED1C24" d="M49.958,17.426V5c0-2.761-2.239-5-5-5H14.875c-2.761,0-5,2.239-5,5v12.509l19.958,9.846L49.958,17.426z
			 M17.875,11.083V9.917c0-1.104,0.896-2,2-2h20.083c1.104,0,2,0.896,2,2v1.167c0,1.104-0.896,2-2,2H19.875
			C18.771,13.083,17.875,12.188,17.875,11.083z"/>
	</g>
	</svg>`;
  }
  