// import React,{useState} from 'react';
// import {View, Keyboard, Text, Image} from 'react-native';

// //icons
// import {Feather,Ionicons} from '@expo/vector-icons';
// import {
//     StyledContainer,
//     InnerContainer,
//     PageLogo,
//     PageTitle,
//     SubTitle,
//     StyledFormArea,
//     LeftIcon,
//     StyledTextInput,
//     StyledInputLabel,
//     RightIcon,
//     Colors
// }
// from '../../../components/Login/Styles'
// import { StatusBar } from 'react-native';
// import {Formik} from 'formik';


// const {brand, darkLight} =Colors;

// const Login = () =>{
//     const [hidePassword, setHidePassword]=useState(true)
//     return (
//       <StyledContainer>
//           <StatusBar style='dark'/>
//         <InnerContainer>
//             <PageLogo resizMode='contain' source={require('../../../assets/img/destination.png')}/>    
//             <PageTitle>Travel</PageTitle>
//             <SubTitle>Account Login</SubTitle>

//             <Formik
//                 initialValues={{email: '',password: ''}}
//                 onSubmit ={(values) =>{
//                     console.log(values);

//                 }}
//             >{({handleChange, handleBlur,handleSubmit,values})=><StyledFormArea>
//                 <MyTextInput
//                     label ='Email Address'
//                     icon = 'mail'
//                     placeholder ='ez123@gmail.com'
//                     placeholderTextColor={darkLight}
//                     onChangeText={handleChange('email')}
//                     onBlur={handleBlur('email')}
//                     value={values.email}
//                     keyboardType ='email-address'
//                 />
//                 <MyTextInput
//                     label ='Password'
//                     icon = 'password'
//                     placeholder ='**********'
//                     placeholderTextColor={darkLight}
//                     onChangeText={handleChange('password')}
//                     onBlur={handleBlur('password')}
//                     value={values.password}
//                     secureTextEntry={hidePassword}
//                     isPassword={true}
//                     hidePassword ={hidePassword}
//                     setHidePassword={setHidePassword}
//                 />
//             </StyledFormArea>

//             }

//             </Formik>
//         </InnerContainer>
//       </StyledContainer>

//     );
// }

// const MyTextInput = ({label,isPassword, icon,hidePassword,setHidePassword, ...props}) =>{
//     return (
//         <View>
//             <LeftIcon>
//                 {/* <Octicons name={icon} size={30} color={brand}/> */}
              
//             </LeftIcon>
//             <StyledInputLabel>{label}</StyledInputLabel>
//             <StyledTextInput {...props}/>
//             {isPassword && (
//                 <RightIcon>
//                     {/* <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'} size={30} color={darkLight}/> */}
//                 </RightIcon>
//             )}
//         </View>
//     )
// }

// export default Login;




// // 'use strict';

// // import {Dimensions} from 'react-native';
// // const width90 = (Dimensions.get('window').width * 90) / 100;
// // const React = require('react-native');
// // let {StyleSheet} = React;
// // const textInput = {
// //   width: width90,
// //   alignSelf: 'center',
// //   borderBottomWidth: 1,
// //   borderColor: 'white',
// //   paddingHorizontal: 10,
// //   color: 'white',
// //   fontFamily: 'Roboto-Bold',
// // };
// // module.exports = StyleSheet.create({
// //   rootContainerStyle: {
// //     flex: 1,
// //     backgroundColor: '#3b4252',
// //   },
// //   textInputStyle: Object.assign(textInput, {
// //     marginBottom: 10,
// //   }),
// //   textInputLoginStyle: Object.assign(textInput, {
// //     marginBottom: 10,
// //   }),
// //   buttonStyle: {
// //     marginTop: 20,
// //     height: 50,
// //     width: width90,
// //     backgroundColor: 'red',
// //   },
// //   labelContainer: {
// //     alignSelf: 'center',
// //     flexDirection: 'row',
// //     marginBottom: 20,
// //   },
// //   labelFirst: {
// //     color: 'white',
// //     fontSize: 20,
// //     fontFamily: 'Roboto-Bold',
// //     marginRight: 5,
// //   },
// //   labelSecond: {
// //     color: 'red',
// //     fontSize: 20,
// //     fontFamily: 'Roboto-Bold',
// //   },
// //   loaderStyle: {
// //     width: 50,
// //     height: 50,
// //     backgroundColor: 'red',
// //   },
// //   image: {
// //     width: 65,
// //     height: 72,
// //     alignSelf: 'center',
// //   },
// //   Atitle: {
// //     width: '100%',
// //     fontFamily: 'Roboto-Medium',
// //     fontSize: 18,
// //     textAlign: 'center',
// //     color: 'black',
// //     marginVertical: '5%',
// //   },
// //   Abody: {
// //     width: '80%',
// //     color: '#616161',
// //     fontSize: 14,
// //     alignSelf: 'center',
// //     textAlign: 'center',
// //     marginBottom: '10%',
// //   },
// //   AlertModal: {
// //     padding: 20,
// //     borderRadius: 10,
// //     backgroundColor: 'white',
// //     justifyContent: 'center',
// //     paddingVertical: 40,
// //   },
// //   footerText: {
// //     alignSelf: 'center',
// //     textAlign: 'center',
// //     width: '90%',
// //     fontSize: 14,
// //     color: '#7f879d',
// //     position: 'absolute',
// //     bottom: 10,
// //   },
// // });



// import styled from 'styled-components';
// import {View, Text, Image,TextInput,TouchableOpacity} from 'react-native';
// // import Constants from 'expo-constants'
// //colors

// // const StatusBarHeight = Constants.statusBarHeight;

// export const Colors={
//   primary : '#ffffff',
//   secondary :'#E5E7E8',
//   tertiary :'#1F2937',
//   darkLight : '#9CA3AF',
//   brand : 'red',
//   green : '#10B981',
//   red :'#EF4444',

// };

// const {primary , secondary,tertiary,darkLight,brand,green,red} = Colors;

// export const StyledContainer = styled.View`
//   flex:1;
//   padding :25px;
//   background-color : ${primary};
// `
// export const InnerContainer = styled.View`
//   flex : 1;
//   width: 100%;
//   align-items : center
// `;

// export const PageLogo =styled.Image`
//   width :100px;
//   height : 100px;
//   marginTop:10

// `;

// export const PageTitle =styled.Text`
// font-size:30px;
// text-align : center;
// font-weight :bold;
// color: ${brand};
// padding :10px;
// marginTop:20

// `;
// export const SubTitle = styled.Text`
//   font-size:18px;
//   margin-bottom:20px;
//   letter-spacing:1px;
//   font-weight: bold ;
//   color: ${tertiary};
// `;
// export const StyledFormArea =styled.View`
//   width:90%;
// `;

// export const StyledTextInput = styled .TextInput`
//     background-color: ${secondary};
//     padding: 15px;
//     padding-left :55px;
//     padding-right:55px;
//     border-radius:5px;
//     font-size:16px;
//     height:60px;
//     margin-vertical:10px;
//     margin-bottom:10px;
//     color :${tertiary};
// `;

// export const StyledInputLabel = styled.Text`
//   color:${tertiary};
//   font-size:13px;
//   text-align: left;

// `;

// export const LeftIcon =styled.View`
//   left:15px;
//   top:38px;
//   position :absolute;
//   z-index:1;

// `;
// export const RightIcon =styled.TouchableOpacity`
//   right:15px;
//   top:38px;
//   position : absolute;
//   z-index:1;

// `;

// export const StyledBottom =styled.TouchableOpacity`
//   padding :15px;
//   background-color:${brand};
//   justify-content: center;
//   border-radius : 5px;
//   margin-vertical:5px;
//   height:60px;

// `;
// export const ButtonText =styled.Text`
//   color:${primary};
//   font-size:16px;
// `;




