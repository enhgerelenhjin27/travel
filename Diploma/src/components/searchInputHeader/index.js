import React, {useState} from 'react';
import { 
  Pressable, 
  TextInput,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Text,
  RefreshControlBase

} from 'react-native';
import styles from './Styles';
import Icon from 'react-native-vector-icons/FontAwesome5'
import Animated ,{Easing} from 'react-native-reanimated';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { search } from '../../../assets/svg_icons'; 
import { SvgXml } from 'react-native-svg';

const {Value,timing}=Animated

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
//Search component

const SearchComp = ({
 
}) => {
  const [buttonFocused, setbuttonFocus] = useState(false);
  const [search, setSearch] = useState('');
  const [text, onChangeText] = useState('');
  const [focused, setFocused] = useState(false);

  //animation values 
  const input_box_translate_x= new Value (width)
  const back_button_opacity= new Value(0)
  const content_translate_y=new Value(height)
  const content_opacity =new Value(0)

 
  onFocus=() => {
    setFocused(true);
    //animation
    //inputbox
    const input_box_translate_x_config=easing={
      duration:200,
      toValue :1,
      // easing: Easing,inOut(Easing.ease)
    }
    //back
    const back_button_opacity_config=easing={
      duration:200,
      toValue :1,
      // easing: Easing,inOut(Easing.ease)
    }
    //content
    const content_translate_y_config=easing={
      duration:0,
      toValue :0,
      // easing: Easing,inOut(Easing.ease)
    }
    //content-opacity
    const content_opacity_config=easing={
      duration:200,
      toValue :1,
      // easing: Easing,inOut(Easing.ease)
    }
    // run animation 
    timing(input_box_translate_x,input_box_translate_x_config).start()
    timing(back_button_opacity,back_button_opacity_config).start()
    timing(content_translate_y,content_translate_y_config).start()
    timing(content_opacity,content_opacity_config).start()

    refs.input.focus()
  }
  onBlur=() => {
    setFocused(false);
    //animation
    //inputbox
    const input_box_translate_x_config=easing={
      duration:200,
      toValue :width,
      // easing: Easing,inOut(Easing.ease)
    }
    //back
    const back_button_opacity_config=easing={
      duration:50,
      toValue :0,
      // easing: Easing,inOut(Easing.ease)
    }
    //content
    const content_translate_y_config=easing={
      duration:0,
      toValue :height,
      // easing: Easing,inOut(Easing.ease)
    }
    //content-opacity
    const content_opacity_config=easing={
      duration:200,
      toValue :0,
      // easing: Easing,inOut(Easing.ease)
    }
    // run animation 
    timing(input_box_translate_x,input_box_translate_x_config).start()
    timing(back_button_opacity,back_button_opacity_config).start()
    timing(content_translate_y,content_translate_y_config).start()
    timing(content_opacity,content_opacity_config).start()

    //force blur
     refs.input.blur()
  }

  return (
    < >
      
      <SafeAreaView style={styles.header_safe_area} > 
        <View style={styles.header}>
            <View style={styles.header_inner}>
                <TouchableOpacity
                    activeOpacity={1}
                    underlayColor={'#ccd0d5'}
                    onPress={buttonFocused}
                    style={styles.search_icon_box}

>
                             <SvgXml
                              xml={search('black')}
                              width={15}
                              height={15}
                             style={[styles.user_icon]}
                                 />
                </TouchableOpacity>
                <Animated.View
                style={[styles.input_box , {transform:[{ translateX : input_box_translate_x}] }]}
                >
                  <Animated.View
                  style={{opacity:back_button_opacity}}
                  >
                    <TouchableHighlight
                      activeOpacity={1}
                      underlayColor={'#ccd0d5'}
                      onPress={onBlur}
                      style={styles.back_icon_box}
                    >
                       <Icon name='chevron-left' size={22} color='#000000'/>
                    </TouchableHighlight>
                  </Animated.View>
                  <TextInput
                    
                    placeholder= 'Мэдээллээ хайна уу'
                    clearButtonMode ="always"
                    onChangeText={onChangeText}
                    value={text}
                    style={styles.Textinput}
                   
  />

                </Animated.View>
              </View>
          </View>
        </SafeAreaView>

        <Animated.View
          style={[styles.content,{opacity: content_opacity, transform:[{translateY: content_translate_y}]}]}
        >
          <SafeAreaView  style={styles.content_safe_area}>
              <View  style={styles.content_inner}>
                <View  style={styles.separator}/>
                {

                  text === ''
                  ?
                    <View style={styles.image_placeholder_container}>
                      <Image
                      source={require('../../assets/img/search.jpg')}
                      style={styles.image_placeholder}
                      /> 
                      <Text style={styles.image_placeholder_text}>
                        enter
                      </Text>
                    </View>
                  :
                  <ScrollView>
                      <View    style={styles.search_item}>
                        <Icon name='search' size={16} color='#cccccc'/>
                        <Text style={styles.item_icon}>fake result</Text>
                        <Text style={styles.item_icon}>fake result</Text>
                        <Text style={styles.item_icon}>fake result</Text>
                      </View>
                  </ScrollView>
                }
              </View>
          </SafeAreaView>

        </Animated.View>
     
    </>
  );
};
export default SearchComp;






// import React, {useState} from 'react';
// import {View, Pressable, TextInput} from 'react-native';
// import Feather from 'react-native-vector-icons/Feather';
// import styles from './Styles';
// import {SvgXml} from 'react-native-svg';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import search from '../../assets/svg_icons'
// const SearchComp = ({
//   onClearPress = () => {},
//   onChangeText = () => {},
//   onButtonPress = () => {},
//   showExtraButton = false,
//   inputContainerStyle = {},
//   placeHolder = 'Хайх...',

  

//   inputIcon = 'search',
// }) => {
//   const [inputFocused, setFocusInput] = useState(false);
//   const [buttonFocused, setbuttonFocus] = useState(false);
//   const [search, setSearch] = useState('');

//   return (
//     <View style={styles.searchFilterContainer}>
//       <View
//         style={[
//           inputFocused ? styles.searchContainerActive : styles.searchContainer,
//           inputContainerStyle,
//         ]}>
           
//         <TextInput
//           value={search}
//           placeholder={placeHolder}
//           placeholderTextColor={'#d1d1d1'}
//           onChangeText={(text) => {
//             setSearch(text);
//             onChangeText(text);
//           }}
//           style={styles.searchInput}
//           onFocus={() => setFocusInput(true)}
//           onBlur={() => setFocusInput(false)}
//         />
//         <FontAwesome
//           onPress={() => {
//             onClearPress();
//           }}
//           style={styles.iconStyle}
//           size={25}
//           name={inputIcon}
//           color={inputFocused ? 'gray' : '#d1d1d1'}
//         />
//       </View>
//       {showExtraButton && (
//         <Pressable
//           onPress={() => {
//             setbuttonFocus(!buttonFocused);
//             onButtonPress();
//             //   setModalType('show_group');
//           }}
//           style={
//             buttonFocused ? styles.iconContainerActive : styles.iconContainer
//           }>
//           {extraButtonIcon}
//         </Pressable>
//       )}
//     </View>
//   );
// };
// export default SearchComp;

