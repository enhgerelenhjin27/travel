// import {StyleSheet} from 'react-native';
// import {Dimensions} from 'react-native';

//  // Calculate window  size
//  export const width = Dimensions.get('window').width;
//  export const height = Dimensions.get('window').height;

// export const input = () => {
//   return {
//     width: '85%',
//     flexDirection: 'row',
//     borderRadius: 5,
//     borderWidth: 1.5,
//     alignSelf: 'center',
//     height: 40,
//     justifyContent: 'space-between',
//     paddingHorizontal: 10,
//   };
// };
// export default StyleSheet.create({
 
//   searchFilterContainer: {
//     width: '90%',
//     flexDirection: 'row',
//     alignSelf: 'center',
//     justifyContent: 'space-between',
//     marginBottom: 20,
//   },
//   searchContainer: Object.assign(input(), {borderColor: '#d1d1d1'}),
//   searchContainerActive: Object.assign(input(), {borderColor: 'gray'}),
//   searchInput: {
//     height: 40,
//     flex: 1,
//     flexGrow: 0.9,
//     fontFamily: 'Roboto-Bold',
//   },
//   iconStyle: {
//     alignSelf: 'center',
//   },
//   iconContainer: {
//     width: 40,
//     height: 40,
//     borderColor: '#d1d1d1',
//     borderRadius: 5,
//     borderWidth: 2,
//     justifyContent: 'center',
//   },
//   iconContainerActive: {
//     width: 40,
//     height: 40,
//     borderColor: 'gray',
//     borderRadius: 5,
//     borderWidth: 2,
//     backgroundColor: '#d1d1d1',
//     justifyContent: 'center',
//   },

//   header_safe_area:{
//     zIndex:1000,
//   },
//   header:{
//     height:50,
//     paddingHorizontal:16,

//   },
//   header_inner:{
//     flex:1,
//     overflow:'hidden',
//     flexDirection:'row',
//     justifyContent:'space-between',
//     alignItems:'center',
//     position:'relative',

//   },
//   search_icon_box:{
//     width:40,
//     height:40,
//     borderRadius:40,
//     backgroundColor:'#e4e6eb',
//     flexDirection:'row',
//     justifyContent:'center',
//     alignItems:'center'
//   },
//   input_box:{
//     height:50,
//     flexDirection:'row',
//     alignItems:'center',
//     position:'absolute',
//     top:0,
//     left:0,
//     backgroundColor:'white',
//     width: width -32,
//   },
//   back_icon_box:{
//     width:40,
//     height:40,
//     borderRadius:40,
//     flexDirection:'row',
//     justifyContent:'center',
//     alignItems:'center',
//     marginRight:5
//   },
//   Textinput:{
//     flex:1,
//     height:400,
//     backgroundColor:'#e4e6eb',
//     borderRadius:16,
//     paddingHorizontal:16,
//     fontSize:15
//   },
//   Content:{
//     width:width,
//     height:height,
//     position:'absolute',
//     left:0,
//     bottom:0,
//     zIndex:999,
//   },
//   content_safe_area:{
//     flex:1,
//     backgroundColor:'white',

//   },
//   content_inner:{
//     flex:1,
//     paddingTop:50,
//   },
//   separator:{
//     marginTop:5,
//     height:1,
//     backgroundColor:'#e4e6eb'

//   },
//   image_placeholder_container:{
//       flex:1,
//       flexDirection:'column',
//       justifyContent:'center',
//       marginTop:'-50%',

//   },

//   image_placeholder:{

//     width:150,
//     height:113,
//     alignSelf:'center'
//   },
//   image_placeholder_text:{
//     textAlign:'center',
//     color:'gray',
//     marginTop:5,

//   },
//   search_item:{
//     flex:1,
//     height:40,
//     alignItems:'center',
//     borderBottomWidth:1,
//     borderBottomColor:'#e4e6eb',
//     marginleft:16
//   },
//   item_icon:{
//     marginLeft:15
//   },

// });
import {StyleSheet} from 'react-native';
export const input = () => {
  return {
    width: '85%',
    flexDirection: 'row',
    borderRadius: 5,
    borderWidth: 1.5,
    alignSelf: 'center',
    height: 40,
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  };
};
export default StyleSheet.create({
  searchFilterContainer: {
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  searchContainer: Object.assign(input(), {borderColor: '#d1d1d1'}),
  searchContainerActive: Object.assign(input(), {borderColor: 'gray'}),
  searchInput: {
    height: 40,
    flex: 1,
    flexGrow: 0.9,
    fontFamily: 'Roboto-Bold',
  },
  iconStyle: {
    alignSelf: 'center',
  },
  iconContainer: {
    width: 40,
    height: 40,
    borderColor: '#d1d1d1',
    borderRadius: 5,
    borderWidth: 2,
    justifyContent: 'center',
  },
  iconContainerActive: {
    width: 40,
    height: 40,
    borderColor: 'gray',
    borderRadius: 5,
    borderWidth: 2,
    backgroundColor: '#d1d1d1',
    justifyContent: 'center',
  },
});
