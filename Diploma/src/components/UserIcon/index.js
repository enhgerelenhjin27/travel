/* eslint-disable prettier/prettier */
import React from 'react';
import {View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {SvgXml} from 'react-native-svg';
import {squirlce,squirlce_border,squircleBG} from './utils/svg_mask';
var styles = require('./Styles');


const UserIcon = ({
  path = require('./utils/people.png'),
  width = 40,
  height = 40,
  style = {},
  backgrounColor = '#ECECEC',
}) => {

  const isImageAcceptable = (url) => {
    const newUrl = url !== undefined && url !== null && url.includes('http') ?
      {
        uri: url,
        priority: FastImage.priority.high,
      } : require('./utils/people.png');
    return newUrl;
  };

  return (
    <View style={[{width: width,height: height,alignSelf: 'center', marginVertical:30,marginStart:10,},style]}>
            <FastImage
              style={[styles.userIcon,{width: width,height: height,}]}
              source={path.uri ? isImageAcceptable(path.uri) : require('./utils/people.png')}
              resizeMode={FastImage.resizeMode.stretch}
      />
       <SvgXml
            width={width}
            height={height}
            style={{position:'absolute'}}
            rx={20}
            xml={squircleBG(backgrounColor)}/>
         <SvgXml
            width={width}
            height={height}
            style={{position:'absolute'}}
            rx={20}
        xml={squirlce_border('rgba(0,0,0,0.08)')} />
    </View>
  );
};
export default UserIcon;
