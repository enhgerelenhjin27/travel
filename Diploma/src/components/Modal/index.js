import React from 'react';
import {
StyleSheet,Text, View, TouchableOpacity, Dimensions, BackHandler
} from 'react-native'
import FastImage from 'react-native-fast-image';


const WIDTH = Dimensions.get('window').width;
const HEIGHT_MODAL =550;

const Modal_Info=(props) =>{
        closeModal =(bool, data) =>{
            props.changeModalVisible(bool);
            props.setData(data);
        }
    return(
        <View>
        <TouchableOpacity
        disable = {true}
        style={styles.container}
        >
            <View style={styles.modal}>
            <Text style={{alignSelf:'center', fontWeight:'bold', fontSize:14}}>{'Хорго хэмээх газар Архангай аймгийн Тариат сумын тєвд оршдог.'}</Text>

            <FastImage
                    style={{flex:1, width:250,height:100, marginHorizontal:10}}
                    source={
                        require('../../assets/img/horgiin_togoi_uul.jpg')
                      }
                 />
               
            <Text style={styles.textItem}>{'    Энэ нь манай орны хамгийн сүүлд унтарсан галт уул бөгөөд жаахан хэтрүүлбэл уур нь савсдаг байна.\nХорго уул нь далайн тєвшнєєс дээш 2240 метрийн єндєр.\n Тогоон дотор зєвхєн баруун талаас нь л маш болгоомжтой орж болно.\nБас урагш зургаан километр орчимд Суман голын зvvн гар талд Тариат сумын тєвєєс баруун урагш 3 орчим километр зайтай газарт чулуун гэр гэж сонирхолтой vзмэр бий.\n   Хоргын өөр нэг гайхамшиг бол тэндхийн хvрмэн чулуун гадаргын завсар зайд ургасан намхан загзгар хуш модны самар болон бусад жимс нэн элбэг байдаг явдал.Бас хvрмэн чулуунд агуй хонгил элбэг.'}</Text>
          
            <View >
                <TouchableOpacity style={styles.Cancel}
                    // onPress={() => closeModal(false, 'Cancel')}
                >
                    <Text style={styles.Cancel_text}>Cancel</Text>
                </TouchableOpacity>
            </View>
            
            </View>
        </TouchableOpacity>
        </View>
    )

}
const styles = StyleSheet.create({
    container :{
        flex:1,
        alignItems:'center',
        justifyContent :'center'
    },
    modal : {
        height:HEIGHT_MODAL,
        width:WIDTH-80,
        paddingTop:10,         
        backgroundColor:'white',
        borderRadius:10
    },
    Cancel:{
        width:60, 
        height:30, 
        backgroundColor:'gray', 
        marginLeft:"75%", 
        borderRadius:5, 
        marginBottom:5, 
        alignItems:'center'
    },
    textItem:{
        alignSelf:'stretch', 
        fontSize:14, 
        marginHorizontal:10,
        paddingBottom:10, 
        paddingTop:10
    },
    Cancel_text:{
        alignItems:'center', 
        paddingTop:5,
        fontWeight:'bold', 
        fontSize:14
    }

})
export default Modal_Info;