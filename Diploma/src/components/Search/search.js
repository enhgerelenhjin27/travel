import React, {useState} from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import { search } from '../../assets/svg_icons'; 
import { SvgXml } from 'react-native-svg';
import axios from 'axios';
import styles from './Style';

const Search = ({value, onValueChange, onFinishEnter}) =>{
    const [text, onChangeText] = useState('');
    return (
        <View style= {styles.searchPanel}>
            {/* <SvgXml
                              xml={search('black')}
                              width={15}
                              height={15}
                              flexDirection={'row'}
                             style={{flexDirection:'row'}}
                                 /> */}
              <TextInput
                    value={value}
                    onChangeText={onValueChange}
                    placeholder= 'Мэдээллээ хайна уу'
                    clearButtonMode ="always"
                    style={{flexDirection:'row'}}
                    onEndEditing={onFinishEnter}
                   
  />
        </View>
    );
} ;
export default Search ;