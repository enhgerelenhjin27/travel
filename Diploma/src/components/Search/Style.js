import{StyleSheet}from 'react-native';
module.exports = StyleSheet.create({
searchPanel:{
    marginHorizontal:20,
    alignItems: 'center',
    backgroundColor: '#CAD5E2',
    borderRadius:7,
    height:40,
    // width:70
  },
  searchText:{
    color:"#f4f4f4f4",
  fontSize:14
  },
  search_icon:{
      paddingLeft:-10
  }
})