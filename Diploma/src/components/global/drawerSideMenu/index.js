import React, {useState} from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import styles from './Styles';
import {View, Text, ScrollView, Pressable, Image} from 'react-native';
import {SvgXml} from 'react-native-svg';
// import {set} from '../../../utils/async';
import { home ,
        heart,
        userInfo,
        log_out,
} from '../../../assets/svg_icons';
import User from '../User';
import favorite from '../../../assets/img/favorite.png'

const CustomDrawerContent = ({navigation}) => {
  const [array, setArray] = useState(drawerItems);
  const [latestTest, setLatestTest] = useState([]);
  const [drawerItems, setDrawerData] = useState([
  {
    name: 'Бүгд',
    screen:'Бүх мэдээлэл',
    icon:home,
    focused: true,
    iconColor: '#4081C2',
   
  },
  {
    name: 'Дуртай газар',
    screen:'Дуртай газар',
    icon:heart,
    focused: false,
    iconColor: '#8667AC',
   
  },
  {
    name: 'Миний мэдээлэл',
    screen:'Миний мэдээлэл',
    icon:userInfo,
    focused: false,
    iconColor: '#4BAF48',
   
  },
]);
// useEffect(() => {
//   const array = state.drawerItems.item;
//   let newArray = [];
//   array.map((item, ind) => {
//     if (ind < 10) {
//       newArray.unshift(item);
//     }
//   });
//   setLatestTest(newArray);
// }, [state.testList.dashboardData]);

const drawerItemStyle = (pressed, focused) => {
  if (pressed) {
    return styles.drawerItemStylePressed;
  } else {
    if (focused) {
      return styles.drawerItemStyleFocused;
    } else {
      return styles.drawerItemStyle;
    }
  }
};
const drawerItemLabel = (item, pressed, screen) => {
  return (
    <View style={styles.menuLabelFlex}>
      {/* <SvgXml
        style={{alignSelf: 'center', }}
        width={20}
        height={20}
        xml={item.icon(
          
          pressed ? 'black' : item.focused ? 'white' : item.iconColor,
        )
          }
      
      /> */}
      {/* <Image
     source={require('../../../assets/img/favorite.png')}
     style={{width:20, height:20, color:'red'}}
      /> */}
   
      
   
      <Text
        style={[
          styles.menuTitle,
          {color: pressed ? 'gray' : item.focused ? 'white' : 'black'},
        ]}>
        {item.name.toUpperCase()}
      </Text>
    </View>
  );
};

  return (
 
    <View style={{backgroundColor:"rgb(236, 236, 236 )", flex:1}}>
       <View style={{backgroundColor:"rgb(236, 236, 236 )", height:'20%', marginBottom:10}}>
        <User/>
       </View>
      
    <ScrollView style={{flex: 1 ,backgroundColor:"rgb(236, 236, 236 )"}}> 
         <View style={styles.gestureContainer}>
           
        <View
          style={styles.gestureLineActive }
        />
        
      </View>
    
    {drawerItems.map((item, idx) => (
      
      
          <Pressable
            key={`drawer-item-${idx + 1}`}
            style={({pressed}) => drawerItemStyle(pressed, item.focused)}
            onPress={() => {
              drawerItems.map((obj) => (obj.focused = false));
              drawerItems[idx].focused = true;
              setDrawerData([...drawerItems]);
              navigation.navigate(item.screen);
            }}>
            {({pressed}) => drawerItemLabel(item, pressed)}
          </Pressable>
        ))}
       
    </ScrollView>
       <Pressable
       style={({pressed}) =>
         pressed ? styles.buttonContainerPress : styles.buttonContainer
       }
       onPress={() => {
        //  set('isLggedIn', '0');
        //  dispatch({type: LOGOUT});
     
       }}>
       <SvgXml
         style={{alignSelf: 'center', marginRight: 15}}
         width={20}
         height={20}
         xml={log_out()}
       />
       <Text style={styles.logOutText}>{'Гарах'}</Text>
     </Pressable>
     </View>
  );
};
export default CustomDrawerContent;