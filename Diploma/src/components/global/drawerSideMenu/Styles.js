import {StyleSheet} from 'react-native';

const buttonContainer = () => {
  return {
    alignItems: 'center',
    bottom: 0,
    alignSelf: 'center',
    marginBottom: 30,
    marginTop: 20,
    width: '80%',
    height: 45,
    borderRadius: 5,
    justifyContent: 'center',
    flexDirection: 'row',
  };
};
const gestureLine = () => {
  return {
    height: '100%',
    width: 7,
    borderRadius: 5,
    backgroundColor: 'white',
    alignSelf: 'flex-end',
  };
};

const drawerItem = () => {
  return {
    height: 45,
    justifyContent: 'center',
    width: '80%',
    alignSelf: 'center',
    backgroundColor: 'white',
    borderWidth: 2,
    padding: 0,
    borderColor: 'rgba(0,0,0,0.2)',
    borderRadius: 5,
    marginBottom: 10,
    paddingHorizontal: 10,
  };
};

export default StyleSheet.create({
  containerStyle: {
    padding: 0,
    backgroundColor: 'white',
    paddingTop: 40,
    flex: 1,
  },
  menuTitle: {
    marginLeft: 10,
    color: 'black',
    alignSelf: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 14,
    height: 20,
  },
  logOutText: {
    color: 'black',
    alignSelf: 'center',
    fontSize: 16,
    fontFamily: 'Roboto-Bold',
    marginBottom: 2,
  },
  menuLabelFlex: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexGrow: 1,
  },
  logout: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 5,
  },
  userName: {
    color: 'black',
    fontSize: 18,
    fontFamily: 'Roboto-Bold',
    alignSelf: 'center',
    marginTop: 10,
  },
  appName: {
    color: 'gray',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    alignSelf: 'center',
  },
  divider: {
    borderBottomColor: 'white',
    opacity: 0.2,
    borderBottomWidth: 1,
    marginBottom: 20,
  },
  gradientProgress: {
    width: 100,
    height: 100,
    alignSelf: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  progressValue: {
    color: '#4756b8',
    fontFamily: 'Roboto-Bold',
    alignSelf: 'center',
    position: 'absolute',
    fontSize: 18,
  },
  progressLabel: {
    color: 'black',
    fontFamily: 'Roboto-Bold',
    alignSelf: 'center',
    fontSize: 15,
    marginVertical: 10,
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginBottom: 10,
  },
  itemValue: {
    width: '30%',
    color: 'black',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 12,
    flexDirection: 'row',
  },
  itemProgressValue: {
    color: 'black',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 12,
  },
  itemProgress: {
    backgroundColor: 'green',
    height: 10,
    alignSelf: 'center',
    marginHorizontal: 2,
    borderRadius: 5,
  },
  itemTitle: {
    width: '30%',
    color: 'black',
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
  },
  drawerItemStyle: Object.assign(drawerItem(), {
    backgroundColor: 'white', //sectionButtonsUnFocused BackgroundColor
  }),
  drawerItemStyleFocused: Object.assign(drawerItem(), {
    backgroundColor: '#75BDC1', //sectionButtonsFocused BackgroundColor
    borderColor: '#70B2B6',  
  }),
  drawerItemStylePressed: Object.assign(drawerItem(), {
    backgroundColor: '#B3D1D3', //sectionButtonsPressed BackgroundColor
  }),
  buttonContainer: Object.assign(buttonContainer(), {
    backgroundColor: 'rgb(236, 236, 236 )', //logout button backgroundcolor
  }),
  buttonContainerPress: Object.assign(buttonContainer(), {
    backgroundColor: '#B3D1D3', //logout button pressedBackgroundColor
  }),
  infoText: {
    textAlign: 'center',
    alignSelf: 'center',
    width: '100%',
    color: 'gray',
    fontFamily: 'Roboto-Medium',
  },
  gestureContainer: {
    position: 'absolute',
    alignSelf: 'center',
    height: 60,
    backgroundColor: 'transparent',
    top: '45%',
    width: '110%',
    justifyContent: 'flex-end',
  },
  gestureLine: Object.assign(gestureLine(), {
    backgroundColor: 'gray',
  }),
  gestureLineActive: Object.assign(gestureLine(), {
    backgroundColor: 'white',
  }),
});
