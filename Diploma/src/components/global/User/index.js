import React from 'react'
import {Text, View,TouchableOpacity} from 'react-native'
import UserIcon from '../../UserIcon'
import styles from './Style'
const User =()=>{
    return(
        <View style={styles.container}>  
        <Text style={styles.Name}>Г.Золжаргал</Text>
        <TouchableOpacity style={styles.Circle}>
            <UserIcon />
        </TouchableOpacity>
        </View>
    )
}
export default User