import { StyleSheet} from 'react-native';
module.exports=StyleSheet.create({
  
    Circle:{
        width:40,
        height:40,
        borderRadius:10,
        marginTop:8,
        marginHorizontal:30
        
    },
    
    Status:{
        position:'absolute',
        marginLeft:100,
        marginTop:20,
    
    },
    Name:{
        position:'absolute',
        marginLeft:100,
        fontSize:16,
        marginTop:50,
        fontFamily:'Roboto-Bold'
        
    }
})