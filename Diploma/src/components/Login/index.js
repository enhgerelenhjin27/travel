import React, { useEffect, useState, useRef } from "react";
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Animated,
  Pressable,
  Image,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import {SvgXml} from 'react-native-svg';
import Styles from "./Styles";
import {Actions} from 'react-native-router-flux';
import { login_user,login_password } from "../../assets/svg_icons";

const Login =({navigation})=>{
    const [username,password]=useState("")
    const [text, onChangeText] = useState("");
    const moveText = useRef(new Animated.Value(0)).current;
    const [number, onChangeNumber] = useState(null);

    // validate_field=()=>{
    //     const {username, password}=useState('')

    //     if(username == ''){
    //         alert ("Хэрэглэгчийн нэр оруулна уу")
    //         return false
    //     }else if(password == ''){
    //         alert ("Хэрэглэгчийн нууц үгээ оруулна уу")
    //         return false
    //     } return true
    // }

    // making_api_call =()=>{
    //     if(this.validate_field()){
    //         alert('Амжиллтай нэвтэрлээ')
    //     }
    // }

    const userInfo ={username:'admin', password :'ez12345'};
 
  goNav=()=>{
        Actions.home()
    }
  signup=()=>{
        Actions.signUP()
    }
return (
    <View 
     style={[Styles.mainContainer]}
    >
     
        <Image
        source={require('../../assets/img/destination.png')}
        style={[Styles.AppLogo]}
        resizeMode="center"
        />
        
     <Text 
      style={[Styles.HeaderText]}
      >Эх орноо тольдоё</Text> 
       

        <TextInput 
        svgPath
        placeholder={"ez2022@gmail.com"}
        style={[Styles.textInput]}
        onChangeText={onChangeText}
        value={text}
        selectionColor='red'
        keyboardType='email-address'
        />
        <SvgXml
             xml={login_user('gray')}
             width={15}
             height={15}
             style={[Styles.Log_user_icon]}
        />

        <TextInput 
        placeholder={"************"}
        style={[Styles.textInput2]}
        onChangeText={onChangeNumber}
        secureTextEntry={true}
        selectionColor='red'
        />
           <SvgXml
             xml={login_password('gray')}
             width={15}
             height={15}
             style={[Styles.Log_user_icon]}
        />

        <View style={{marginTop:'10%', width:'70%', }}>
        <TouchableOpacity
            style={[Styles.LoginButton]}
            onPress={()=>goNav()}
        >
            <Text style={{color:'white', fontWeight:'bold', fontSize:16, paddingHorizontal:10}}>Нэвтрэх</Text>
        </TouchableOpacity>
        <TouchableOpacity
            style={[Styles.SignUp]}       
            onPress={()=>signup()}
            >
            <Text style={{color:'red', fontWeight:'bold',fontSize:16}}>Бүртгүүлэх</Text>
            
        </TouchableOpacity>
    </View>
    </View>

)
}
export default Login;