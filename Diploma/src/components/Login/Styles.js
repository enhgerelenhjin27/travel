import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    width:'100%', 
    height:'100%',
    justifyContent:'center', 
    alignSelf:'center', 
    alignContent:'center',
    alignItems:'center',
    borderRadius:30
  },
  mainContainer1: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    width:'100%', 
    height:'100%',
    justifyContent:'center', 
    alignSelf:'center', 
    alignContent:'center',
    alignItems:'center'
  },

  AppLogo:{
    width:100,
    height:100,
    marginTop:-50, 
    marginVertical:20
  },
  HeaderText:{
    textAlign:'center' ,
    fontWeight:"bold" ,
    fontSize:24,
    color:'black', 
    paddingHorizontal:10, 
    marginTop:10
  },
  textInput:{
    fontSize:15,
    fontWeight:'bold',
    height:42,
    width:"80%", 
    borderBottomWidth:1,
    marginTop:'5%',
    
  },
  textInput2:{
    fontSize:15,
    fontWeight:'bold',
    height:42,
    width:"80%", 
    borderBottomWidth:1,
    
  },
  LoginButton:{
    height:42, 
    width:'70%',
    textAlign:'center',
    justifyContent:'center',
    alignSelf:"center",
    alignItems:'center',
    borderRadius:40,
    backgroundColor:'#4081C2', 
    marginTop:10
  },
  SignUp:{
    height:42, 
    width:'70%',
    textAlign:'center',
    justifyContent:'center',
    alignSelf:"center",
    alignItems:'center',
    borderRadius:40,
    marginTop:10
  },
  Log_user_icon:{
   right:-120, 
   top:-30,
  
  },
 
});