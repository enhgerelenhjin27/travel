import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    width:'100%', 
    height:'100%',
    justifyContent:'center', 
    alignSelf:'center', 
    alignContent:'center',
    alignItems:'center',
    
    
  },
HeaderText:{
  textAlign:'center' ,
  fontWeight:"bold" ,
  fontSize:24,
  color:'#FFE7D7', 
  paddingHorizontal:10, 
  marginTop:20
},
  textInput:{
    height:42,
    width:"80%", 
    backgroundColor:'#ECECEC',
    borderRadius:10,
    marginTop:10,
    paddingLeft:15,
    fontSize:15,
    fontWeight:'bold',
    
   
  },
 
  SignUp:{
    height:45, 
    width:'80%',
    textAlign:'center',
    backgroundColor:'#B7F5EF',
    justifyContent:'center',
    alignSelf:"center",
    alignItems:'center',
    borderRadius:40,
    marginTop:10,
    
  },
  backButton:{
    height:42, 
    width:'70%',
    textAlign:'center',
    justifyContent:'center',
    alignSelf:"center",
    alignItems:'center',
    borderRadius:40,
    marginTop:10
  },
  user_icon:{
    right:-120, 
    top:-30,
   },
});