import React, { useEffect, useState, useRef } from "react";
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Animated,
  Pressable,
  Image,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import {SvgXml} from 'react-native-svg';
import  { user,email, login_password} from '../../assets/svg_icons';
import {Actions} from 'react-native-router-flux';
import Styles from "./Style";

const SignUP =(props)=>{
    const [text, onChangeText] = useState("");
    const moveText = useRef(new Animated.Value(0)).current;
    const [number, onChangeNumber] = useState(null);
    const [hidePassword, setHidePassword]=useState(true)

 goBack=()=>{
  Actions.pop()
 }
return (
    <ImageBackground 
    source={require('../../assets/img/background.jpg')}
     style={[Styles.mainContainer]}
    >
      <Text 
      style={[Styles.HeaderText]}
      >Шинэ хэрэглэгчээр бүртгүүлэх</Text> 
       
        <TextInput 
        placeholder={"Нэр :"}
        style={[Styles.textInput]}
        onChangeText={onChangeText}
        selectionColor='#C0BFBF'
        // value={text}
        />
        <SvgXml
             xml={user('black')}
             width={15}
             height={15}
            style={[Styles.user_icon]}
        />
        <TextInput 
        placeholder={"Овог :"}
        style={[Styles.textInput]}
        onChangeText={onChangeText}
        selectionColor='#C0BFBF'
        />
        <SvgXml
             xml={user('black')}
             width={15}
             height={15}
            style={[Styles.user_icon]}
        />
        <TextInput 
        placeholder={"Имэйл хаяг :"}
        style={[Styles.textInput]}
        onChangeText={onChangeText}
        selectionColor='#C0BFBF'
        />
          <SvgXml
             xml={email('gray')}
             width={15}
             height={15}
            style={[Styles.user_icon]}
        />
       
        <TextInput 
        placeholder={"Нууц үг оруулах :"}
        style={[Styles.textInput]}
        onChangeText={onChangeNumber}
        secureTextEntry={true}
        selectionColor='#C0BFBF'
        />
          <SvgXml
             xml={login_password('gray')}
             width={15}
             height={15}
            style={[Styles.user_icon]}
        />
        <TextInput 
        placeholder={"Нууц үг давтаж оруулах :"}
        style={[Styles.textInput]}
        onChangeText={onChangeNumber}
        secureTextEntry={true}
        selectionColor='#C0BFBF'
        /> 

        <SvgXml
             xml={login_password('gray')}
             width={15}
             height={15}
            style={[Styles.user_icon]}
        />

        <View style={{marginTop:'10%', width:'70%', }}>
       
        <TouchableOpacity
            style={[Styles.SignUp]}       
            onPress={()=>alert('you clicked')}
            >
            <Text style={{color:'black', fontWeight:'bold',fontSize:16}}>Бүртгүүлэх</Text>
            
        </TouchableOpacity>

        <TouchableOpacity
            style={[Styles.backButton]}       
            onPress={()=>goBack()}
            >
            <Text style={{color:'white', fontWeight:'bold',fontSize:16}}>Буцах</Text>
            
        </TouchableOpacity>
    </View>
    </ImageBackground>

)
}
export default SignUP;