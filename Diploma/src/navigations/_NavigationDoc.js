import React from 'react';
import { View } from "react-native";
import{Stack} from './NavigationUtils'
import Complain from '../screen/Doc/Complain/index';

const Navigation = ()=>{
return(
    <Stack.Navigator>
    <Stack.Screen
    name="doc"
    component={Complain}
    options={{headerShown:false,animationEnabled:false}}
    />
</Stack.Navigator>
)
}
export default Navigation