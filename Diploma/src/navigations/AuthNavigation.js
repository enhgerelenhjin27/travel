import React from 'react';
import {Stack} from './NavigationUtils';
import Login from '../components/Login';

const AuthNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
export default AuthNavigation
