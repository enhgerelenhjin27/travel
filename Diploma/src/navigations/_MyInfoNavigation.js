import React from 'react';
import {Stack} from './NavigationUtils';
import MyInfoScreen from '../screen/MyInfo/index';

const MyInfoNavigation = () => {
  return (
    <Stack.Navigator>
    <Stack.Screen
      name="public"
      component={MyInfoScreen}
      options={{headerShown: false, animationEnabled: false}}
    />
  
  </Stack.Navigator>
  );
};
export default MyInfoNavigation;
