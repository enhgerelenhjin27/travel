import React from 'react';
import {Stack} from './NavigationUtils';
import FavBox from '../screen/FavBox';
const FavNavigation = () => {
  return (
    <Stack.Navigator>
    <Stack.Screen
      name="FavBox"
      component={FavBox}
      options={{headerShown: false, animationEnabled: false}}
    />
  
  </Stack.Navigator>
  );
};
export default FavNavigation;
