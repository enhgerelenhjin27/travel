import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import NavigationDoc from './_NavigationDoc';
import FavNavigation from './_FavNavigation';
import MyInfoNavigation from './_MyInfoNavigation';
import DrawerSideMenu from "../components/global/drawerSideMenu";
import { Drawer } from './NavigationUtils';


const MyDrawer = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <DrawerSideMenu navigation={props.navigation} />}>
      <Drawer.Screen name="Бүх мэдээлэл" component={NavigationDoc} />
      <Drawer.Screen name="Дуртай газар" component={FavNavigation} />
      <Drawer.Screen name="Миний мэдээлэл" component={MyInfoNavigation} />
    </Drawer.Navigator>
    
  );
};

const AppNavigation = () => {
  return (
    <NavigationContainer>
      <MyDrawer />
    </NavigationContainer>
  );
};
export default AppNavigation;
