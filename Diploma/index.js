/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

// require('dotenv').config();
// import {startServer} from './server';

// startServer();

AppRegistry.registerComponent(appName, () => App);
